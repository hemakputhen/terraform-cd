provider "aws" {
  region = "us-east-1"
}
terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "terraformstatefilehkpapr8"
    key = "terraform.tfstate"
    dynamodb_table = "terraform"
  }
}
